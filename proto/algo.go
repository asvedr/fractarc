package proto

import (
	"fractarc/entities/rect"
	"fractarc/entities/unzipped"
	"fractarc/entities/zipped"
	"image"
)

// impls.piece_comparer
// type IPieceComparer interface {
// 	CheckEq(a rect.IRect, b rect.IRect) bool
// }

type IHasher interface {
	Hash(rect rect.IRect) string
}

type ITiler interface {
	MakeTiles(img image.Image, side int) unzipped.Tiled
}

type IMerger interface {
	Merge(tiles []rect.IRect, tiles_in_row int) image.Image
}

type ILCompressor interface {
	Zip(image.Image, unzipped.Tiled) zipped.Layer
}

type ILDecompressor interface {
	Unzip(zipped.Layer) image.Image
}
