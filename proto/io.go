package proto

import (
	// "fractarc/entities/archive"
	"fractarc/entities/zipped"
	"image"
)

type IImgLoader interface {
	Load(path string) (image.Image, error)
}

type IImgSaver interface {
	Save(img image.Image, path string) error
}

type IImgIO interface {
	IImgLoader
	IImgSaver
}

type IArchEncoder interface {
	ToBytes(zipped.Arch) ([]byte, error)
	FromBytes([]byte) (*zipped.Arch, error)
}

type IArchLoader interface {
	Load(path string) (zipped.Layer, error)
}

type IArchSaver interface {
	Save(arc zipped.Layer, path string) error
}

type IArchiveIO interface {
	IArchLoader
	IArchSaver
}
