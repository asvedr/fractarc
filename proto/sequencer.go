package proto

import (
	"fractarc/entities/rect"
	"image"
)

// impls.sequencer
type ISequencer interface {
	Iter(img image.Image, tile_side int) ISequenceIter
}

// impls.sequencer
type ISequenceIter interface {
	Rect() rect.IRect
	Next() bool
}
