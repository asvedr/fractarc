package check_patches

import (
	"errors"
	"fractarc/app"
	"fractarc/entities/zipped"
	"image"
	"image/color"

	"gitlab.com/asvedr/cldi/cl"
)

type EP struct{}

type request struct {
	Path      string `short:"p"`
	Side      int    `short:"s"`
	MaskColor string `short:"m" default:"red"`
}

var colors = map[string]color.Color{
	"red":   color.RGBA{R: 255, A: 255},
	"green": color.RGBA{G: 255, A: 255},
	"blue":  color.RGBA{B: 255, A: 255},
}

func (EP) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "patch",
		Description: "check patch coverage",
		ParamParser: cl.MakeParser[request](),
	}
}

func (EP) Execute(prog string, args any) error {
	req := args.(request)
	color, found := colors[req.MaskColor]
	if !found {
		return errors.New("invalid color")
	}
	io := app.ImgIO.Get()
	tiler := app.Tiler.Get()
	zipper := app.LCompressor.Get()
	unzipper := app.LDecompressor.Get()
	pic, err := io.Load(req.Path)
	if err != nil {
		return err
	}
	tiled := tiler.MakeTiles(pic, req.Side)
	zipped := zipper.Zip(pic, tiled)
	restored := unzipper.Unzip(zipped)
	mark_repeats(&zipped, find_repeats(zipped), color)
	masked := unzipper.Unzip(zipped)
	return errors.Join(
		io.Save(masked, req.Path+".masked.png"),
		io.Save(restored, req.Path+".restored.png"),
	)
}

func find_repeats(arch zipped.Layer) []int {
	counts := map[int]int{}
	for _, tile := range arch.Tiles {
		if tile.IsRng() {
			from, to := tile.Rng()
			for i := int(from); i < int(to); i += 1 {
				counts[i] += 1
			}
		} else {
			counts[int(tile.Tile())] += int(tile.Count())
		}
	}
	result := []int{}
	for tile, cnt := range counts {
		if cnt > 1 {
			result = append(result, tile)
		}
	}
	return result
}

func mark_repeats(arch *zipped.Layer, repeats []int, pix color.Color) {
	side := int(arch.Header.TileSide)
	pic := arch.Pic.(*image.RGBA)
	for _, i := range repeats {
		pt := image.Point{X: i * side}
		polish(pic, pt, side, pix)
	}
}

func polish(pic *image.RGBA, shift image.Point, side int, pix color.Color) {
	for y := range side {
		for x := range side {
			pic.Set(x+shift.X, y+shift.Y, pix)
		}
	}
}
