package main

import (
	_ "fractarc/app"
	"fractarc/entrypoints/check_patches"

	"gitlab.com/asvedr/cldi/cl"
)

func main() {
	cl.NewRunnerMulty(
		"dbg",
		"",
		check_patches.EP{},
	).Run()
}
