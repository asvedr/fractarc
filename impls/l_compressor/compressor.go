package l_compressor

import (
	"fractarc/entities/rect"
	"fractarc/entities/unzipped"
	"fractarc/entities/zipped"
	"fractarc/proto"
	"image"
)

type zipper struct{}

func New() proto.ILCompressor {
	return zipper{}
}

func (self zipper) Zip(img image.Image, tiled unzipped.Tiled) zipped.Layer {
	header := prepare_header(img, tiled)
	unused_tiles := find_unused_tiles(tiled)
	shift_map := make_shift_map(tiled, unused_tiles)
	pic := prepare_zipped_pic(tiled, unused_tiles)
	tiles := make_tile_map(tiled, shift_map)
	var result zipped.Layer
	result.Pic = pic
	result.Header = header
	result.Tiles = tiles
	return result
}

func prepare_header(img image.Image, tiled unzipped.Tiled) zipped.Header {
	limits := img.Bounds()
	var zero image.Point
	if img.Bounds().Min != zero {
		panic("image min != 0,0")
	}
	return zipped.Header{
		ImgWidth:      uint16(limits.Max.X),
		ImgHeight:     uint16(limits.Max.Y),
		TilesInWidth:  uint16(tiled.TilesInRow),
		TilesInHeight: uint16(len(tiled.Map) / tiled.TilesInRow),
		TileSide:      uint16(tiled.TileSide),
	}
}

func find_unused_tiles(tiled unzipped.Tiled) map[int]bool {
	counted := map[int]int{}
	for _, tile := range tiled.Map {
		counted[tile] += 1
	}
	unused := map[int]bool{}
	for i := range tiled.Tiles {
		if counted[i] == 0 {
			unused[i] = true
		}
	}
	return unused
}

func make_shift_map(tiled unzipped.Tiled, unused_tiles map[int]bool) map[int]int {
	removed := 0
	shifts := map[int]int{}
	for i := range tiled.Tiles {
		if unused_tiles[i] {
			removed += 1
			shifts[i] = -1
		} else {
			shifts[i] = i - removed
		}
	}
	return shifts
}

func prepare_zipped_pic(tiled unzipped.Tiled, unused_tiles map[int]bool) image.Image {
	used_count := len(tiled.Tiles) - len(unused_tiles)
	side := tiled.TileSide
	img := image.NewRGBA(
		image.Rectangle{
			Max: image.Point{X: used_count * side, Y: side},
		},
	)
	begin := 0
	for i, tile := range tiled.Tiles {
		if unused_tiles[i] {
			continue
		}
		put_tile(img, tile, begin)
		begin += side
	}
	return img
}

func put_tile(img *image.RGBA, tile rect.IRect, begin int) {
	height := tile.Height()
	width := tile.Width()
	for y := range height {
		for x := range width {
			img.Set(x+begin, y, tile.At(x, y))
		}
	}
}

func make_tile_map(tiled unzipped.Tiled, shift_map map[int]int) []zipped.ITile {
	tile_seq := grouped_to_tile_map(group_tiles(tiled.Map))
	for i, tile := range tile_seq {
		tile_seq[i] = tile.Shift(shift_map)
	}
	return tile_seq
}

func group_tiles(tiles []int) [][]int {
	grouped := [][]int{}
	last_index := -1
	for _, tile := range tiles {
		if last_index == tile {
			last_i := len(grouped) - 1
			grouped[last_i] = append(grouped[last_i], tile)
		} else {
			grouped = append(grouped, []int{tile})
			last_index = tile
		}
	}
	return grouped
}

func grouped_to_tile_map(grouped [][]int) []zipped.ITile {
	result := []zipped.ITile{}

	prev_tile := -2
	started_at := -2

	dump_rep := func(group []int) {
		tile := zipped.NewSingleTile(uint16(group[0]), uint16(len(group)))
		result = append(result, tile)
	}

	dump_seq := func() {
		if started_at < 0 {
			return
		}
		if started_at == prev_tile {
			dump_rep([]int{started_at})
			prev_tile = -2
			started_at = -2
			return
		}
		tile := zipped.NewRngTile(uint16(started_at), uint16(prev_tile))
		result = append(result, tile)
		prev_tile = -2
		started_at = -2
	}

	for _, group := range grouped {
		if len(group) == 1 {
			// single
			tile := group[0]
			if tile == prev_tile+1 {
				// in seq
				prev_tile = tile
			} else {
				// new seq
				dump_seq()
				started_at = tile
				prev_tile = tile
			}
		} else {
			// block of repeat
			dump_seq()
			dump_rep(group)
		}
	}
	dump_seq()
	return result
}
