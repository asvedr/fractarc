package l_compressor_test

import (
	"fractarc/entities/rect"
	"fractarc/entities/zipped"
	"fractarc/impls/hasher"
	"fractarc/impls/l_compressor"
	"fractarc/impls/sequencer"
	"fractarc/impls/tiler"
	"image"
	"image/color"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

type Hasher struct{}

func (Hasher) Hash(rect rect.IRect) string {
	res := ""
	for y := range rect.Height() {
		for x := range rect.Width() {
			r := rect.At(x, y).(color.RGBA).R
			res += string(rune(r))
		}
	}
	return res
}

func cl(red ...int) []color.Color {
	res := []color.Color{}
	for _, r := range red {
		res = append(res, color.RGBA{R: uint8(r)})
	}
	return res
}

func make_pic(rows ...[]color.Color) image.Image {
	w := len(rows[0])
	h := len(rows)
	img := image.NewRGBA(image.Rect(0, 0, w, h))
	for y, row := range rows {
		for x, clr := range row {
			img.Set(x, y, clr)
		}
	}
	return img
}

func TestRepeated(t *testing.T) {
	pic := make_pic(
		cl(11, 12, 13, 14, 15, 16),
		cl(21, 22, 23, 24, 25, 26),
		cl(31, 32, 33, 34, 35, 36),
		cl(41, 42, 43, 11, 12, 13),
		cl(51, 52, 53, 21, 22, 23),
		cl(61, 62, 63, 31, 32, 33),
	)
	tiler := tiler.New(hasher.New(1), sequencer.New())
	tiled := tiler.MakeTiles(pic, 3)
	arch := l_compressor.New().Zip(pic, tiled)

	exp_pic := make_pic(
		cl(11, 12, 13, 14, 15, 16, 41, 42, 43),
		cl(21, 22, 23, 24, 25, 26, 51, 52, 53),
		cl(31, 32, 33, 34, 35, 36, 61, 62, 63),
	)
	exp_header := zipped.Header{
		ImgWidth:      6,
		ImgHeight:     6,
		TilesInWidth:  2,
		TilesInHeight: 2,
		TileSide:      3,
	}
	exp_tiles := []zipped.ITile{
		zipped.NewRngTile(0, 2),
		zipped.NewSingleTile(0, 1),
	}

	assert.Equal(t, exp_pic, arch.Pic)
	assert.Equal(t, exp_header, arch.Header)
	assert.Equal(t, exp_tiles, arch.Tiles)
}

func TestRepeatMoreThen1(t *testing.T) {
	pic := make_pic(
		cl(11, 12, 13, 11, 12, 13),
		cl(21, 22, 23, 21, 22, 23),
		cl(31, 32, 33, 31, 32, 33),
		cl(41, 42, 43, 11, 12, 13),
		cl(51, 52, 53, 21, 22, 23),
		cl(61, 62, 63, 31, 32, 33),
	)
	tiler := tiler.New(hasher.New(1), sequencer.New())
	tiled := tiler.MakeTiles(pic, 3)
	arch := l_compressor.New().Zip(pic, tiled)

	exp_pic := make_pic(
		cl(11, 12, 13, 41, 42, 43),
		cl(21, 22, 23, 51, 52, 53),
		cl(31, 32, 33, 61, 62, 63),
	)
	exp_header := zipped.Header{
		ImgWidth:      6,
		ImgHeight:     6,
		TilesInWidth:  2,
		TilesInHeight: 2,
		TileSide:      3,
	}
	exp_tiles := []zipped.ITile{
		zipped.NewSingleTile(0, 2),
		zipped.NewSingleTile(1, 1),
		zipped.NewSingleTile(0, 1),
	}
	assert.Equal(t, exp_pic, arch.Pic)
	assert.Equal(t, exp_header, arch.Header)
	assert.Equal(t, exp_tiles, arch.Tiles)
}

func TestMixedRepeat(t *testing.T) {
	pic := make_pic(
		cl(11, 12, 13, 14, 11, 12),
		cl(21, 22, 23, 24, 21, 22),
		cl(31, 32, 11, 12, 35, 36),
		cl(41, 42, 21, 22, 45, 46),
		cl(35, 36, 53, 21, 22, 23),
		cl(45, 46, 63, 31, 32, 33),
	)
	tiler := tiler.New(Hasher{}, sequencer.New())
	tiled := tiler.MakeTiles(pic, 2)
	arch := l_compressor.New().Zip(pic, tiled)

	expected := []zipped.ITile{
		zipped.NewRngTile(0, 1),
		zipped.NewSingleTile(0, 1),
		zipped.NewSingleTile(2, 1),
		zipped.NewSingleTile(0, 1),
		zipped.NewSingleTile(3, 2),
		zipped.NewRngTile(4, 5),
	}
	if !reflect.DeepEqual(arch.Tiles, expected) {
		t.Fatalf(
			"different seqs\n(%d) %v\n(%d) %v",
			tile_len(expected),
			expected,
			tile_len(arch.Tiles),
			arch.Tiles,
		)
	}
}

func tile_len(tiles []zipped.ITile) int {
	res := 0
	for _, tile := range tiles {
		if tile.IsRng() {
			from, to := tile.Rng()
			res += int(to - from + 1)
		} else {
			res += int(tile.Count())
		}
	}
	return res
}
