package hasher_test

import (
	"fractarc/entities/rect"
	"fractarc/impls/hasher"
	"fractarc/proto"
	"image"
	"image/color"
	"testing"

	"github.com/stretchr/testify/assert"
)

func c(r, g, b uint8) color.Color {
	return color.RGBA{R: r, G: g, B: b, A: 255}
}

func cl(clr ...color.Color) []color.Color {
	return clr
}

func make_pic(rows ...[]color.Color) rect.IRect {
	w := len(rows[0])
	h := len(rows)
	img := image.NewRGBA(image.Rect(0, 0, w, h))
	for y, row := range rows {
		for x, clr := range row {
			img.Set(x, y, clr)
		}
	}
	return rect.New(img, image.Point{}, w, h)
}

func new_hasher() proto.IHasher {
	return hasher.New(2)
}

func TestEqualPics(t *testing.T) {
	a := make_pic(
		cl(c(255, 0, 0), c(0, 255, 0), c(0, 0, 255)),
		cl(c(11, 0, 0), c(0, 11, 0), c(0, 0, 11)),
		cl(c(22, 0, 0), c(0, 22, 0), c(0, 0, 22)),
	)
	b := make_pic(
		cl(c(255, 0, 0), c(0, 255, 0), c(0, 0, 255)),
		cl(c(11, 0, 0), c(0, 11, 0), c(0, 0, 11)),
		cl(c(22, 0, 0), c(0, 22, 0), c(0, 0, 22)),
	)
	assert.Equal(
		t,
		new_hasher().Hash(a),
		new_hasher().Hash(b),
	)
}

func TestSamePics(t *testing.T) {
	a := make_pic(
		cl(c(255, 1, 1), c(0, 255, 0), c(0, 0, 255)),
		cl(c(11, 0, 0), c(0, 10, 0), c(0, 0, 11)),
		cl(c(22, 0, 0), c(0, 22, 0), c(0, 0, 22)),
	)
	b := make_pic(
		cl(c(255, 0, 0), c(0, 255, 0), c(0, 0, 255)),
		cl(c(11, 0, 0), c(0, 11, 0), c(0, 0, 11)),
		cl(c(22, 0, 0), c(0, 22, 0), c(0, 0, 23)),
	)
	assert.Equal(
		t,
		new_hasher().Hash(a),
		new_hasher().Hash(b),
	)
}

func TestDiffPics(t *testing.T) {
	a := make_pic(
		cl(c(255, 0, 0), c(0, 255, 0), c(0, 0, 255)),
		cl(c(11, 0, 0), c(0, 1, 0), c(0, 0, 11)),
		cl(c(22, 0, 0), c(0, 22, 0), c(0, 0, 22)),
	)
	b := make_pic(
		cl(c(255, 0, 0), c(0, 255, 0), c(0, 0, 255)),
		cl(c(11, 0, 0), c(0, 11, 0), c(0, 0, 11)),
		cl(c(22, 0, 0), c(0, 22, 0), c(0, 0, 22)),
	)
	ha := new_hasher().Hash(a)
	hb := new_hasher().Hash(b)
	assert.NotEqual(t, ha, hb)
}
