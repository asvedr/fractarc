package hasher

import (
	"fractarc/entities/rect"
	"fractarc/proto"
)

type hasher struct {
	q_mul uint32
}

func New(q_mul int) proto.IHasher {
	if q_mul < 1 {
		q_mul = 1
	}
	return hasher{q_mul: uint32(q_mul)}
}

func (self hasher) Hash(rect rect.IRect) string {
	runes := make([]rune, rect.Square()*3)
	width := rect.Width()
	height := rect.Height()
	for y := range height {
		row := y * width * 3
		for x := range width {
			r, g, b, _ := rect.At(x, y).RGBA()
			shift := row + (x * 3)
			runes[shift] = rune(self.decreace_quality(r >> 8))
			runes[shift+1] = rune(self.decreace_quality(g >> 8))
			runes[shift+2] = rune(self.decreace_quality(b >> 8))
		}
	}
	return string(runes)
}

func (self hasher) decreace_quality(val uint32) uint32 {
	return (val / self.q_mul) * self.q_mul
}
