package imgio

import (
	"fractarc/proto"
	"image"
	"image/png"
	"os"
)

type imgio struct{}

func New() proto.IImgIO {
	return imgio{}
}

func (imgio) Load(path string) (image.Image, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	image, _, err := image.Decode(file)
	return image, err
}

func (imgio) Save(img image.Image, path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()
	return png.Encode(file, img)
}
