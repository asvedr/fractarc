package sequencer_test

import (
	"fractarc/entities/rect"
	"fractarc/impls/hasher"
	"fractarc/impls/sequencer"
	"fractarc/proto"
	"image"
	"image/color"
	"testing"

	"github.com/stretchr/testify/assert"
)

func cl(red ...int) []color.Color {
	res := []color.Color{}
	for _, r := range red {
		res = append(res, color.RGBA{R: uint8(r)})
	}
	return res
}

func make_pic(rows ...[]color.Color) image.Image {
	w := len(rows[0])
	h := len(rows)
	img := image.NewRGBA(image.Rect(0, 0, w, h))
	for y, row := range rows {
		for x, clr := range row {
			img.Set(x, y, clr)
		}
	}
	return img
}

func make_rect(rows ...[]color.Color) []rune {
	img := make_pic(rows...)
	w := len(rows[0])
	h := len(rows)
	return hash(rect.New(img, image.Point{}, w, h))
}

func collect(iter proto.ISequenceIter) [][]rune {
	res := [][]rune{}
	for iter.Next() {
		res = append(res, hash(iter.Rect()))
	}
	return res
}

func hash(rect rect.IRect) []rune {
	return []rune(hasher.New(1).Hash(rect))
}

func TestPic6Side3(t *testing.T) {
	pic := make_pic(
		cl(10, 11, 12, 13, 15, 16),
		cl(20, 21, 22, 23, 25, 26),
		cl(30, 31, 32, 33, 35, 36),
		cl(40, 41, 42, 43, 45, 46),
		cl(50, 51, 52, 53, 55, 56),
		cl(60, 61, 62, 63, 65, 66),
	)
	collected := collect(sequencer.New().Iter(pic, 3))
	expected := [][]rune{
		make_rect(
			cl(10, 11, 12),
			cl(20, 21, 22),
			cl(30, 31, 32),
		),
		make_rect(
			cl(13, 15, 16),
			cl(23, 25, 26),
			cl(33, 35, 36),
		),
		make_rect(
			cl(40, 41, 42),
			cl(50, 51, 52),
			cl(60, 61, 62),
		),
		make_rect(
			cl(43, 45, 46),
			cl(53, 55, 56),
			cl(63, 65, 66),
		),
	}
	assert.Equal(t, expected, collected)
}

func TestPic5Side3(t *testing.T) {
	pic := make_pic(
		cl(10, 11, 12, 13, 15),
		cl(20, 21, 22, 23, 25),
		cl(30, 31, 32, 33, 35),
		cl(40, 41, 42, 43, 45),
		cl(50, 51, 52, 53, 55),
	)
	collected := collect(sequencer.New().Iter(pic, 3))
	expected := [][]rune{
		make_rect(
			cl(10, 11, 12),
			cl(20, 21, 22),
			cl(30, 31, 32),
		),
	}
	assert.Equal(t, expected, collected)
}
