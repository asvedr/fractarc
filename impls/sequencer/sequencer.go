package sequencer

import (
	"fractarc/proto"
	"image"
)

type sequencer struct{}

func New() proto.ISequencer {
	return sequencer{}
}

func (self sequencer) Iter(
	img image.Image,
	tile_side int,
) proto.ISequenceIter {
	limit := img.Bounds().Max
	return &iter{
		img:         img,
		point:       image.Point{X: -tile_side, Y: 0},
		img_size:    limit,
		tile_width:  tile_side,
		tile_height: tile_side,
	}
}
