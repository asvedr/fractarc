package sequencer

import (
	"fractarc/entities/rect"
	"image"
)

type iter struct {
	img         image.Image
	point       image.Point
	img_size    image.Point
	tile_width  int
	tile_height int
}

func (self *iter) Rect() rect.IRect {
	return rect.New(
		self.img,
		self.point,
		self.tile_width,
		self.tile_height,
	)
}

func (self *iter) Next() bool {
	self.shift(self.tile_width, 0)
	if !self.out_of_bounds() {
		return true
	}
	self.point.X = 0
	self.shift(0, self.tile_height)
	return !self.out_of_bounds()
}

func (self *iter) shift(dx, dy int) {
	self.point.X += dx
	self.point.Y += dy
}

func (self *iter) out_of_bounds() bool {
	p := self.point
	i := self.img_size
	return p.X+self.tile_width > i.X || p.Y+self.tile_height > i.Y
}
