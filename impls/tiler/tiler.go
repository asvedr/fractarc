package tiler

import (
	"fractarc/entities/rect"
	"fractarc/entities/unzipped"
	"fractarc/proto"
	"image"
)

type tiler struct {
	hasher    proto.IHasher
	sequencer proto.ISequencer
}

func New(
	hasher proto.IHasher,
	sequencer proto.ISequencer,
) proto.ITiler {
	return tiler{hasher: hasher, sequencer: sequencer}
}

func (self tiler) MakeTiles(img image.Image, side int) unzipped.Tiled {
	img_width, img_height := find_img_size(img)
	ext_img_width := ext_img_size(img_width, side)
	ext_img_height := ext_img_size(img_height, side)
	ext_img := copy_image(img, ext_img_width, ext_img_height)
	iter := self.sequencer.Iter(ext_img, side)
	tiles := []rect.IRect{}
	hash_to_index := map[string][]int{}
	index_to_hash := []string{}
	for iter.Next() {
		tile := iter.Rect()
		index := len(tiles)
		tiles = append(tiles, tile)
		hash := self.hasher.Hash(tile)
		hash_to_index[hash] = append(hash_to_index[hash], index)
		index_to_hash = append(index_to_hash, hash)
	}
	return unzipped.Tiled{
		TileSide:   side,
		TilesInRow: ext_img_width / side,
		Tiles:      tiles,
		Map:        fold_tiles(index_to_hash, hash_to_index),
	}
}

func find_img_size(img image.Image) (int, int) {
	bnds := img.Bounds()
	return bnds.Dx(), bnds.Dy()
}

func ext_img_size(img_side, tile_side int) int {
	for ; img_side%tile_side > 0; img_side += 1 {
	}
	return img_side
}

func fold_tiles(
	index_to_hash []string,
	hash_to_index map[string][]int,
) []int {
	result := make([]int, len(index_to_hash))
	for i, hash := range index_to_hash {
		result[i] = hash_to_index[hash][0]
	}
	return result
}

func copy_image(img image.Image, ext_width int, ext_height int) image.Image {
	res := image.NewRGBA(
		image.Rectangle{
			Min: image.Point{X: 0, Y: 0},
			Max: image.Point{X: ext_width, Y: ext_height},
		},
	)
	bnds := img.Bounds()
	if bnds.Min.X != 0 {
		panic("min x > 0")
	}
	if bnds.Min.Y != 0 {
		panic("min y > 0")
	}
	limits := bnds.Max
	for y := range limits.Y {
		for x := range limits.X {
			res.Set(x, y, img.At(x, y))
		}
	}
	return res
}
