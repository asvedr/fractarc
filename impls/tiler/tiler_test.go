package tiler_test

import (
	"fractarc/entities/rect"
	"fractarc/impls/hasher"
	"fractarc/impls/sequencer"
	"fractarc/impls/tiler"
	"fractarc/proto"
	"image"
	"image/color"
	"testing"

	"github.com/stretchr/testify/assert"
)

func cl(red ...int) []color.Color {
	res := []color.Color{}
	for _, r := range red {
		res = append(res, color.RGBA{R: uint8(r)})
	}
	return res
}

func make_pic(rows ...[]color.Color) image.Image {
	w := len(rows[0])
	h := len(rows)
	img := image.NewRGBA(image.Rect(0, 0, w, h))
	for y, row := range rows {
		for x, clr := range row {
			img.Set(x, y, clr)
		}
	}
	return img
}

func hash(r rect.IRect) []rune {
	return []rune(hasher.New(1).Hash(r))
}

func make_rect(rows ...[]color.Color) []rune {
	img := make_pic(rows...)
	w := len(rows[0])
	h := len(rows)
	return hash(rect.New(img, image.Point{}, w, h))
}

func new_tiler() proto.ITiler {
	return tiler.New(hasher.New(1), sequencer.New())
}

func TestPic6Tile3(t *testing.T) {
	pic := make_pic(
		cl(11, 12, 13, 14, 15, 16),
		cl(21, 22, 23, 24, 25, 26),
		cl(31, 32, 33, 34, 35, 36),
		cl(41, 42, 43, 44, 45, 46),
		cl(51, 52, 53, 54, 55, 56),
		cl(61, 62, 63, 64, 65, 66),
	)
	tiled := new_tiler().MakeTiles(pic, 3)
	assert.Equal(t, tiled.TileSide, 3)
	assert.Equal(t, tiled.TilesInRow, 2)
	assert.Equal(t, tiled.Map, []int{0, 1, 2, 3})
	assert.Equal(t, len(tiled.Tiles), 4)
	assert.Equal(
		t,
		hash(tiled.Tiles[0]),
		make_rect(cl(11, 12, 13), cl(21, 22, 23), cl(31, 32, 33)),
	)
	assert.Equal(
		t,
		hash(tiled.Tiles[1]),
		make_rect(cl(14, 15, 16), cl(24, 25, 26), cl(34, 35, 36)),
	)
	assert.Equal(
		t,
		hash(tiled.Tiles[2]),
		make_rect(cl(41, 42, 43), cl(51, 52, 53), cl(61, 62, 63)),
	)
	assert.Equal(
		t,
		hash(tiled.Tiles[3]),
		make_rect(cl(44, 45, 46), cl(54, 55, 56), cl(64, 65, 66)),
	)
}

func TestPic5Tile3(t *testing.T) {
	pic := make_pic(
		cl(11, 12, 13, 14, 15),
		cl(21, 22, 23, 24, 25),
		cl(31, 32, 33, 34, 35),
		cl(41, 42, 43, 44, 45),
		cl(51, 52, 53, 54, 55),
	)
	tiled := new_tiler().MakeTiles(pic, 3)
	assert.Equal(t, tiled.TileSide, 3)
	assert.Equal(t, tiled.TilesInRow, 2)
	assert.Equal(t, tiled.Map, []int{0, 1, 2, 3})
	assert.Equal(t, len(tiled.Tiles), 4)
	assert.Equal(
		t,
		hash(tiled.Tiles[0]),
		make_rect(cl(11, 12, 13), cl(21, 22, 23), cl(31, 32, 33)),
	)
	assert.Equal(
		t,
		hash(tiled.Tiles[1]),
		make_rect(cl(14, 15, 0), cl(24, 25, 0), cl(34, 35, 0)),
	)
	assert.Equal(
		t,
		hash(tiled.Tiles[2]),
		make_rect(cl(41, 42, 43), cl(51, 52, 53), cl(0, 0, 0)),
	)
	assert.Equal(
		t,
		hash(tiled.Tiles[3]),
		make_rect(cl(44, 45, 0), cl(54, 55, 0), cl(0, 0, 0)),
	)
}

func TestPicRepeats(t *testing.T) {
	pic := make_pic(
		cl(11, 12, 13, 14, 15, 16),
		cl(21, 22, 23, 24, 25, 26),
		cl(31, 32, 33, 34, 35, 36),
		cl(41, 42, 43, 11, 12, 13),
		cl(51, 52, 53, 21, 22, 23),
		cl(61, 62, 63, 31, 32, 33),
	)
	tiled := new_tiler().MakeTiles(pic, 3)
	assert.Equal(t, tiled.TileSide, 3)
	assert.Equal(t, tiled.TilesInRow, 2)
	assert.Equal(t, tiled.Map, []int{0, 1, 2, 0})
	assert.Equal(t, len(tiled.Tiles), 4)
	assert.Equal(
		t,
		hash(tiled.Tiles[0]),
		make_rect(cl(11, 12, 13), cl(21, 22, 23), cl(31, 32, 33)),
	)
	assert.Equal(
		t,
		hash(tiled.Tiles[1]),
		make_rect(cl(14, 15, 16), cl(24, 25, 26), cl(34, 35, 36)),
	)
	assert.Equal(
		t,
		hash(tiled.Tiles[2]),
		make_rect(cl(41, 42, 43), cl(51, 52, 53), cl(61, 62, 63)),
	)
	assert.Equal(
		t,
		hash(tiled.Tiles[3]),
		make_rect(cl(11, 12, 13), cl(21, 22, 23), cl(31, 32, 33)),
	)
}
