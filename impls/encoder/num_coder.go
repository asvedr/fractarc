package encoder

import (
	"bytes"
	"encoding/binary"
)

type i_num_coder interface {
	Encode(uint16) []byte
	Decode(*bytes.Buffer) (uint16, error)
}

type u8_coder struct{}
type u16_coder struct {
	bin binary.ByteOrder
	buf []byte
}

func new_u8_coder() i_num_coder {
	return u8_coder{}
}

func new_u16_coder(bin binary.ByteOrder) i_num_coder {
	return u16_coder{
		bin: bin,
		buf: []byte{0, 0},
	}
}

func (u8_coder) NewBuffer() []byte {
	return []byte{0}
}

func (u8_coder) Encode(num uint16) []byte {
	return []byte{byte(num)}
}

func (u8_coder) Decode(buf *bytes.Buffer) (uint16, error) {
	bt, err := buf.ReadByte()
	return uint16(bt), err
}

func (u16_coder) NewBuffer() []byte {
	return []byte{0, 0}
}

func (self u16_coder) Encode(num uint16) []byte {
	buf := []byte{0, 0}
	self.bin.PutUint16(buf, num)
	return buf
}

func (self u16_coder) Decode(buf *bytes.Buffer) (uint16, error) {
	_, err := buf.Read(self.buf)
	return self.bin.Uint16(self.buf), err
}
