package encoder

import "fractarc/entities/zipped"

type num_headers struct {
	index zipped.NumSize
	count zipped.NumSize
}

func (self num_headers) to_byte() byte {
	var res int
	if self.index == zipped.U16 {
		res |= 2
	}
	if self.count == zipped.U16 {
		res |= 1
	}
	return byte(res)
}

func (self *num_headers) from_byte(src byte) {
	as_int := int(src)
	if as_int&2 != 0 {
		self.index = zipped.U16
	} else {
		self.index = zipped.U8
	}
	if as_int&1 != 0 {
		self.count = zipped.U16
	} else {
		self.count = zipped.U8
	}
}
