package encoder_test

import (
	"fractarc/entities/imgenc"
	"fractarc/entities/zipped"
	"fractarc/impls/encoder"
	"image"
	"image/color"
	"testing"

	"github.com/stretchr/testify/assert"
)

func enc_dec(arch zipped.Arch) ([]byte, *zipped.Arch, error) {
	enc := encoder.New(100)
	bts, err := enc.ToBytes(arch)
	if err != nil {
		return nil, nil, err
	}
	img, err := enc.FromBytes(bts)
	return bts, img, err
}

func cl(red ...int) []color.Color {
	res := []color.Color{}
	for _, r := range red {
		res = append(res, color.RGBA{R: uint8(r), A: 255})
	}
	return res
}

func make_pic(rows ...[]color.Color) image.Image {
	w := len(rows[0])
	h := len(rows)
	img := image.NewRGBA(image.Rect(0, 0, w, h))
	for y, row := range rows {
		for x, clr := range row {
			img.Set(x, y, clr)
		}
	}
	return img
}

func hash(img image.Image) []int {
	res := []int{}
	for y := range img.Bounds().Max.Y {
		for x := range img.Bounds().Max.X {
			r, _, _, _ := img.At(x, y).RGBA()
			res = append(res, int(r>>8))
		}
	}
	return res
}

func TestEncDecOneByte(t *testing.T) {
	arch := zipped.Arch{
		Layers: []zipped.LayerMeta{
			{
				Tiles: []zipped.ITile{
					zipped.NewRngTile(0, 5),
					zipped.NewSingleTile(2, 10),
				},
			},
			{
				Header: zipped.Header{
					ImgWidth:      10,
					ImgHeight:     20,
					TilesInWidth:  30,
					TilesInHeight: 40,
					TileSide:      50,
				},
				Tiles: []zipped.ITile{
					zipped.NewSingleTile(1, 2),
					zipped.NewRngTile(60, 70),
				},
			},
		},
		Enc: imgenc.Png,
		Pic: make_pic(
			cl(10, 30, 60),
			cl(90, 120, 150),
			cl(180, 210, 240),
		),
	}
	bts, restored, err := enc_dec(arch)
	assert.Nil(t, err)
	assert.Equal(t, 130, len(bts))
	assert.Equal(t, arch.Enc, restored.Enc)
	assert.Equal(t, arch.Layers, restored.Layers)
	assert.Equal(t, hash(arch.Pic), hash(restored.Pic))
}

func TestEncDecTwoBytes(t *testing.T) {
	arch := zipped.Arch{
		Layers: []zipped.LayerMeta{
			{
				Tiles: []zipped.ITile{
					zipped.NewRngTile(0, 500),
					zipped.NewSingleTile(400, 10),
				},
			},
			{
				Header: zipped.Header{
					ImgWidth:      10,
					ImgHeight:     20,
					TilesInWidth:  30,
					TilesInHeight: 40,
					TileSide:      50,
				},
				Tiles: []zipped.ITile{
					zipped.NewSingleTile(1, 2),
					zipped.NewRngTile(60, 70),
				},
			},
		},
		Enc: imgenc.Png,
		Pic: make_pic(
			cl(10, 30, 60),
			cl(90, 120, 150),
			cl(180, 210, 240),
		),
	}
	bts, restored, err := enc_dec(arch)
	assert.Nil(t, err)
	assert.Equal(t, 133, len(bts))
	assert.Equal(t, arch.Enc, restored.Enc)
	assert.Equal(t, arch.Layers, restored.Layers)
	assert.Equal(t, hash(arch.Pic), hash(restored.Pic))
}
