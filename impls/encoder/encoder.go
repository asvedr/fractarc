package encoder

import (
	"bytes"
	"encoding/binary"
	"fractarc/entities/errs"
	"fractarc/entities/imgenc"
	"fractarc/entities/zipped"
	"fractarc/proto"
	"image"
	"image/jpeg"
	"image/png"
)

type encoder struct {
	bin          binary.ByteOrder
	jpeg_quality int
	buf16        []byte
}

func New(jpeg_quality int) proto.IArchEncoder {
	return encoder{
		bin:          binary.LittleEndian,
		jpeg_quality: jpeg_quality,
		buf16:        []byte{0, 0},
	}
}

func (self encoder) ToBytes(arch zipped.Arch) ([]byte, error) {
	out := bytes.NewBuffer([]byte{})
	layers_len, err := strip_u8("len(layers)", len(arch.Layers))
	if err != nil {
		return nil, err
	}
	out.WriteByte(arch.Enc.ToByte())
	out.WriteByte(layers_len)
	for _, layer := range arch.Layers {
		self.encode_layer_header(out, layer.Header)
		if err != nil {
			return nil, err
		}
		num_hdr := num_headers{
			index: layer.IndexSize(),
			count: layer.CountSize(),
		}
		out.WriteByte(num_hdr.to_byte())
		err = self.encode_layer_tiles(
			out,
			num_hdr,
			layer.Tiles,
		)
		if err != nil {
			return nil, err
		}
	}
	err = self.encode_img(out, arch.Enc, arch.Pic)
	return out.Bytes(), err
}

func (self encoder) FromBytes(src []byte) (*zipped.Arch, error) {
	buf := bytes.NewBuffer(src)
	enc_byte, err := buf.ReadByte()
	if err != nil {
		return nil, err
	}
	enc, err := imgenc.FromByte(enc_byte)
	if err != nil {
		return nil, err
	}
	layers_len, err := buf.ReadByte()
	if err != nil {
		return nil, err
	}
	layers := []zipped.LayerMeta{}
	for range int(layers_len) {
		layer, err := self.read_layer(buf)
		if err != nil {
			return nil, err
		}
		layers = append(layers, layer)
	}
	img, err := self.decode_img(buf, enc)
	if err != nil {
		return nil, err
	}
	arch := &zipped.Arch{Layers: layers, Enc: enc, Pic: img}
	return arch, nil
}

func (self encoder) encode_img(
	out *bytes.Buffer,
	enc imgenc.Encoding,
	img image.Image,
) error {
	switch enc {
	case imgenc.Png:
		return png.Encode(out, img)
	case imgenc.Jpg:
		opts := jpeg.Options{Quality: self.jpeg_quality}
		return jpeg.Encode(out, img, &opts)
	case imgenc.Raw:
		panic("raw img is not implemented yet")
	default:
		panic("unreachable statement")
	}
}

func (self encoder) decode_img(
	inp *bytes.Buffer,
	enc imgenc.Encoding,
) (image.Image, error) {
	switch enc {
	case imgenc.Png:
		return png.Decode(inp)
	case imgenc.Jpg:
		return jpeg.Decode(inp)
	case imgenc.Raw:
		panic("raw img is not implemented yet")
	default:
		panic("unreachable statement")
	}
}

func (self encoder) read_layer(inp *bytes.Buffer) (zipped.LayerMeta, error) {
	var res zipped.LayerMeta
	header, err := self.read_layer_header(inp)
	if err != nil {
		return res, err
	}
	var num_hdr num_headers
	num_hdr_bt, err := inp.ReadByte()
	if err != nil {
		return res, err
	}
	num_hdr.from_byte(num_hdr_bt)
	tiles, err := self.read_tiles(inp, num_hdr)
	res.Header = header
	res.Tiles = tiles
	return res, err
}

func (self encoder) encode_layer_header(out *bytes.Buffer, hdr zipped.Header) {
	self.put_u16(out, hdr.ImgWidth)
	self.put_u16(out, hdr.ImgHeight)
	self.put_u16(out, hdr.TilesInWidth)
	self.put_u16(out, hdr.TilesInHeight)
	self.put_u16(out, hdr.TileSide)
}

func (self encoder) read_layer_header(
	inp *bytes.Buffer,
) (zipped.Header, error) {
	var hdr zipped.Header
	var err error
	hdr.ImgWidth, err = self.get_u16(inp)
	if err != nil {
		return hdr, err
	}
	hdr.ImgHeight, err = self.get_u16(inp)
	if err != nil {
		return hdr, err
	}
	hdr.TilesInWidth, err = self.get_u16(inp)
	if err != nil {
		return hdr, err
	}
	hdr.TilesInHeight, err = self.get_u16(inp)
	if err != nil {
		return hdr, err
	}
	hdr.TileSide, err = self.get_u16(inp)
	return hdr, err
}

func (self encoder) encode_layer_tiles(
	out *bytes.Buffer,
	hdrs num_headers,
	tiles []zipped.ITile,
) error {
	tiles_len, err := strip_u16("len(tiles)", len(tiles))
	if err != nil {
		return err
	}
	self.put_u16(out, tiles_len)
	index_coder := self.get_num_coder(hdrs.index)
	count_coder := self.get_num_coder(hdrs.count)
	for _, tile := range tiles {
		if tile.IsRng() {
			out.WriteByte(1)
			from, to := tile.Rng()
			out.Write(index_coder.Encode(from))
			out.Write(index_coder.Encode(to))
		} else {
			out.WriteByte(0)
			out.Write(index_coder.Encode(tile.Tile()))
			out.Write(count_coder.Encode(tile.Count()))
		}
	}
	return nil
}

func (self encoder) read_tiles(inp *bytes.Buffer, hdrs num_headers) ([]zipped.ITile, error) {
	index_coder := self.get_num_coder(hdrs.index)
	count_coder := self.get_num_coder(hdrs.count)
	tiles_len, err := self.get_u16(inp)
	if err != nil {
		return nil, err
	}
	result := []zipped.ITile{}
	for range tiles_len {
		bt, err := inp.ReadByte()
		if err != nil {
			return nil, err
		}
		var tile zipped.ITile
		if bt != 0 {
			tile, err = read_rng_tile(inp, index_coder)
		} else {
			tile, err = read_sin_tile(inp, index_coder, count_coder)
		}
		if err != nil {
			return nil, err
		}
		result = append(result, tile)
	}
	return result, nil
}

func read_rng_tile(inp *bytes.Buffer, coder i_num_coder) (zipped.ITile, error) {
	from, err := coder.Decode(inp)
	if err != nil {
		return nil, err
	}
	to, err := coder.Decode(inp)
	return zipped.NewRngTile(from, to), err
}

func read_sin_tile(
	inp *bytes.Buffer,
	index_coder i_num_coder,
	count_coder i_num_coder,
) (zipped.ITile, error) {
	tile, err := index_coder.Decode(inp)
	if err != nil {
		return nil, err
	}
	count, err := count_coder.Decode(inp)
	return zipped.NewSingleTile(tile, count), err
}

func (self encoder) get_num_coder(size zipped.NumSize) i_num_coder {
	if size == zipped.U8 {
		return new_u8_coder()
	} else {
		return new_u16_coder(self.bin)
	}
}

func (self encoder) put_u16(out *bytes.Buffer, num uint16) {
	self.bin.PutUint16(self.buf16, num)
	out.Write(self.buf16)
}

func (self encoder) get_u16(inp *bytes.Buffer) (uint16, error) {
	_, err := inp.Read(self.buf16)
	if err != nil {
		return 0, err
	}
	return self.bin.Uint16(self.buf16), nil
}

func strip_u8(attr string, num int) (uint8, error) {
	if int(uint8(num)) != num {
		return 0, errs.AttrToBigForU8{Attr: attr, Val: num}
	}
	return uint8(num), nil
}

func strip_u16(attr string, num int) (uint16, error) {
	if int(uint16(num)) != num {
		return 0, errs.AttrToBigForU16{Attr: attr, Val: num}
	}
	return uint16(num), nil
}
