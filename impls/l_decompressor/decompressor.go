package l_decompressor

import (
	"fractarc/entities/rect"
	"fractarc/entities/zipped"
	"fractarc/proto"
	"image"
)

type unzipper struct{}

func New() proto.ILDecompressor {
	return unzipper{}
}

func (unzipper) Unzip(arch zipped.Layer) image.Image {
	hdr := arch.Header
	result := image.NewRGBA(
		image.Rectangle{
			Max: image.Point{X: int(hdr.ImgWidth), Y: int(hdr.ImgHeight)},
		},
	)
	var row, column int
	for _, tile := range arch.Tiles {
		for _, item := range tile_to_seq(hdr, arch.Pic, tile) {
			shift := get_shift(hdr, row, column)
			put_tile(result, item, shift)
			column += 1
			if column >= int(hdr.TilesInWidth) {
				column = 0
				row += 1
			}
		}
	}
	return result
}

func tile_to_seq(hdr zipped.Header, img image.Image, tile zipped.ITile) []rect.IRect {
	if tile.IsRng() {
		from, to := tile.Rng()
		return rng_to_seq(hdr, img, int(from), int(to))
	} else {
		n := int(tile.Tile())
		cnt := int(tile.Count())
		return repeat_to_seq(hdr, img, n, cnt)
	}
}

func rng_to_seq(hdr zipped.Header, img image.Image, from int, to int) []rect.IRect {
	result := []rect.IRect{}
	for i := from; i <= to; i += 1 {
		result = append(result, get_rect(hdr, img, i))
	}
	return result
}

func repeat_to_seq(hdr zipped.Header, img image.Image, tile int, count int) []rect.IRect {
	result := []rect.IRect{}
	src := get_rect(hdr, img, tile)
	for range count {
		result = append(result, src)
	}
	return result
}

func get_rect(hdr zipped.Header, img image.Image, tile int) rect.IRect {
	side := int(hdr.TileSide)
	pt := image.Point{X: tile * side}
	return rect.New(img, pt, side, side)
}

func get_shift(hdr zipped.Header, row int, column int) image.Point {
	return image.Point{
		X: int(hdr.TileSide) * column,
		Y: int(hdr.TileSide) * row,
	}
}

func put_tile(result *image.RGBA, item rect.IRect, shift image.Point) {
	side := item.Width()
	limit := result.Bounds().Max
	for y := range side {
		pic_y := shift.Y + y
		if pic_y >= limit.Y {
			break
		}
		for x := range side {
			pic_x := shift.X + x
			if pic_x >= limit.X {
				break
			}
			result.Set(pic_x, pic_y, item.At(x, y))
		}
	}
}
