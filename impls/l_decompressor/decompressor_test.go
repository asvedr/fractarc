package l_decompressor_test

import (
	"fractarc/entities/rect"
	"fractarc/impls/hasher"
	"fractarc/impls/l_compressor"
	"fractarc/impls/l_decompressor"
	"fractarc/impls/sequencer"
	"fractarc/impls/tiler"
	"image"
	"image/color"
	"testing"

	"github.com/stretchr/testify/assert"
)

func cl(red ...int) []color.Color {
	res := []color.Color{}
	for _, r := range red {
		res = append(res, color.RGBA{R: uint8(r)})
	}
	return res
}

func make_pic(rows ...[]color.Color) image.Image {
	w := len(rows[0])
	h := len(rows)
	img := image.NewRGBA(image.Rect(0, 0, w, h))
	for y, row := range rows {
		for x, clr := range row {
			img.Set(x, y, clr)
		}
	}
	return img
}

func hash(img image.Image) []rune {
	l := img.Bounds().Max
	r := rect.New(img, image.Point{}, l.X, l.Y)
	return []rune(hasher.New(1).Hash(r))
}

func TestUnzRepeated2(t *testing.T) {
	pic := make_pic(
		cl(11, 12, 13, 14),
		cl(21, 22, 23, 24),
		cl(31, 32, 11, 12),
		cl(41, 42, 21, 22),
	)
	tiler := tiler.New(hasher.New(1), sequencer.New())
	tiled := tiler.MakeTiles(pic, 2)
	arch := l_compressor.New().Zip(pic, tiled)
	tile_map := make_pic(
		cl(11, 12, 13, 14, 31, 32),
		cl(21, 22, 23, 24, 41, 42),
	)
	assert.Equal(t, hash(tile_map), hash(arch.Pic))
	restored := l_decompressor.New().Unzip(arch)
	assert.Equal(t, hash(pic), hash(restored))
}

func TestUnzRepeated3(t *testing.T) {
	pic := make_pic(
		cl(11, 12, 13, 14),
		cl(21, 22, 23, 24),
		cl(31, 32, 11, 12),
		cl(41, 42, 21, 22),
	)
	tiler := tiler.New(hasher.New(1), sequencer.New())
	tiled := tiler.MakeTiles(pic, 3)
	arch := l_compressor.New().Zip(pic, tiled)
	restored := l_decompressor.New().Unzip(arch)
	assert.Equal(t, hash(pic), hash(restored))
}

func TestUnzipRepeatMoreThen1(t *testing.T) {
	pic := make_pic(
		cl(11, 12, 13, 11, 12, 13),
		cl(21, 22, 23, 21, 22, 23),
		cl(31, 32, 33, 31, 32, 33),
		cl(41, 42, 43, 11, 12, 13),
		cl(51, 52, 53, 21, 22, 23),
		cl(61, 62, 63, 31, 32, 33),
	)
	tiler := tiler.New(hasher.New(1), sequencer.New())
	tiled := tiler.MakeTiles(pic, 3)
	arch := l_compressor.New().Zip(pic, tiled)

	restored := l_decompressor.New().Unzip(arch)
	assert.Equal(t, hash(pic), hash(restored))
}

func TestUnzipRepeatMoreThen2(t *testing.T) {
	pic := make_pic(
		cl(11, 12, 13, 14, 11, 12),
		cl(21, 22, 23, 24, 21, 22),
		cl(31, 32, 11, 12, 35, 36),
		cl(41, 42, 21, 22, 45, 46),
		cl(35, 36, 53, 21, 22, 23),
		cl(45, 46, 63, 31, 32, 33),
	)
	tiler := tiler.New(hasher.New(1), sequencer.New())
	tiled := tiler.MakeTiles(pic, 2)
	arch := l_compressor.New().Zip(pic, tiled)

	restored := l_decompressor.New().Unzip(arch)
	assert.Equal(t, hash(pic), hash(restored))
}
