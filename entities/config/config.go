package config

type Config struct {
	MaxPix      int `default:"0"`
	MaxClr      int `default:"4"`
	HashMul     int `default:"2"`
	JpegQuality int `default:"50"`
}
