package unzipped

import "fractarc/entities/rect"

type Tiled struct {
	TileSide   int
	TilesInRow int
	Tiles      []rect.IRect
	Map        []int
}
