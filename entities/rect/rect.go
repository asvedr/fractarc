package rect

import (
	"image"
	"image/color"
)

type IRect interface {
	At(x, y int) color.Color
	X() int
	Y() int
	Width() int
	Height() int
	End() image.Point
	Square() int
	// Reflect() IRect
}

type rect struct {
	pt     image.Point
	width  int
	height int
	img    image.Image
}

// Right-to-bottom
// type reflectRTB struct {
// 	rect *rect
// }

func New(
	img image.Image,
	pt image.Point,
	width int,
	height int,
) IRect {
	return &rect{
		pt:     pt,
		width:  width,
		height: height,
		img:    img,
	}
}

// x: [0..Width), y: [0..Height)
func (self *rect) At(x, y int) color.Color {
	x += self.pt.X
	y += self.pt.Y
	return self.img.At(x, y)
}

func (self *rect) X() int      { return self.pt.X }
func (self *rect) Y() int      { return self.pt.Y }
func (self *rect) Width() int  { return self.width }
func (self *rect) Height() int { return self.height }

func (self *rect) End() image.Point {
	x := self.pt.X + self.width
	y := self.pt.Y + self.height
	return image.Point{X: x, Y: y}
}

func (self *rect) Square() int {
	return self.width * self.height
}

// func (self *rect) Reflect() IRect {
// 	return reflectRTB{rect: self}
// }

// func (self reflectRTB) At(x, y int) color.Color {
// 	return self.rect.At(y, x)
// }

// func (self reflectRTB) X() int      { return self.rect.pt.X }
// func (self reflectRTB) Y() int      { return self.rect.pt.Y }
// func (self reflectRTB) Width() int  { return self.rect.width }
// func (self reflectRTB) Height() int { return self.rect.height }

// func (self reflectRTB) End() image.Point {
// 	rect := self.rect
// 	x := rect.pt.X + rect.height
// 	y := rect.pt.Y + rect.width
// 	return image.Point{X: x, Y: y}
// }

// func (self reflectRTB) Square() int {
// 	return self.rect.Square()
// }

// func (self reflectRTB) Reflect() IRect {
// 	return self.rect
// }
