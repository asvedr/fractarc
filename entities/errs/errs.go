package errs

import "fmt"

type AttrToBigForU8 struct {
	Attr string
	Val  int
}

type AttrToBigForU16 struct {
	Attr string
	Val  int
}

type UnexpectedEOF struct{}

func (self AttrToBigForU8) Error() string {
	return fmt.Sprintf(
		"attr '%s' is too big for uint8: %d",
		self.Attr,
		self.Val,
	)
}

func (self AttrToBigForU16) Error() string {
	return fmt.Sprintf(
		"attr '%s' is too big for uint16: %d",
		self.Attr,
		self.Val,
	)
}

func (self UnexpectedEOF) Error() string {
	return "Unexpected EOF"
}
