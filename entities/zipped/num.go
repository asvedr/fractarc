package zipped

type NumSize int

const (
	U8 NumSize = iota
	U16
)
