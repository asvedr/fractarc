package zipped

import (
	"fractarc/entities/imgenc"
	"image"
)

type Header struct {
	ImgWidth      uint16
	ImgHeight     uint16
	TilesInWidth  uint16
	TilesInHeight uint16
	TileSide      uint16
}

type Arch struct {
	Layers []LayerMeta
	Enc    imgenc.Encoding
	Pic    image.Image
}

type LayerMeta struct {
	Header Header
	Tiles  []ITile
}

type Layer struct {
	Pic image.Image // Horizontally merged tiles
	LayerMeta
}

func (self LayerMeta) IndexSize() NumSize {
	for _, tile := range self.Tiles {
		var index uint16
		if tile.IsRng() {
			_, index = tile.Rng()
		} else {
			index = tile.Tile()
		}
		if index > 255 {
			return U16
		}
	}
	return U8
}

func (self LayerMeta) CountSize() NumSize {
	for _, tile := range self.Tiles {
		if tile.IsRng() {
			continue
		}
		if tile.Count() > 255 {
			return U16
		}
	}
	return U8
}
