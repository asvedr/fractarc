package zipped

import "fmt"

type ITile interface {
	IsRng() bool
	Rng() (uint16, uint16)
	Tile() uint16
	Count() uint16
	Shift(map[int]int) ITile
	String() string
}

type tile_single struct {
	tile  uint16
	count uint16
}

type tile_rng struct {
	from uint16
	to   uint16
}

func NewSingleTile(tile uint16, count uint16) ITile {
	return tile_single{tile: tile, count: count}
}

func NewRngTile(from, to uint16) ITile {
	return tile_rng{from: from, to: to}
}

func (tile_single) IsRng() bool {
	return false
}

func (tile_single) Rng() (uint16, uint16) {
	panic("Rng called for tile_single")
}

func (self tile_single) Tile() uint16 {
	return self.tile
}

func (self tile_single) Count() uint16 {
	return self.count
}

func (self tile_single) Shift(smap map[int]int) ITile {
	self.tile = uint16(smap[int(self.tile)])
	return self
}

func (self tile_single) String() string {
	return fmt.Sprintf("sin(t=%d, c=%d)", self.tile, self.count)
}

func (tile_rng) IsRng() bool {
	return true
}

func (self tile_rng) Rng() (uint16, uint16) {
	return self.from, self.to
}

func (tile_rng) Tile() uint16 {
	panic("Tile called for tile_rng")
}

func (tile_rng) Count() uint16 {
	panic("Count called for tile_rng")
}

func (self tile_rng) Shift(smap map[int]int) ITile {
	self.from = uint16(smap[int(self.from)])
	self.to = uint16(smap[int(self.to)])
	return self
}

func (self tile_rng) String() string {
	return fmt.Sprintf("rng(%d..%d)", self.from, self.to)
}
