package imgenc

import "fmt"

type Encoding uint8

const (
	Raw Encoding = iota
	Png
	Jpg
)

var byte_map = map[Encoding]byte{
	Raw: 0,
	Png: 1,
	Jpg: 2,
}

type InvalidEncoding struct{ Val byte }

func (self InvalidEncoding) Error() string {
	return fmt.Sprintf("invalid encoding: %d", self.Val)
}

func (self Encoding) ToByte() byte {
	return byte_map[self]
}

func FromByte(src byte) (Encoding, error) {
	for enc, bt := range byte_map {
		if bt == src {
			return enc, nil
		}
	}
	return 0, InvalidEncoding{Val: src}
}
