package app

import (
	"fractarc/impls/encoder"
	"fractarc/impls/hasher"
	"fractarc/impls/imgio"
	"fractarc/impls/l_compressor"
	"fractarc/impls/l_decompressor"
	"fractarc/impls/sequencer"
	"fractarc/impls/tiler"
	"fractarc/proto"

	"gitlab.com/asvedr/cldi/di"
)

var ImgIO = di.Singleton[proto.IImgIO]{PureFunc: imgio.New}

var Sequencer = di.Singleton[proto.ISequencer]{
	PureFunc: sequencer.New,
}

var Hasher = di.Singleton[proto.IHasher]{
	PureFunc: func() proto.IHasher {
		return hasher.New(Config.Get().HashMul)
	},
}

var Tiler = di.Singleton[proto.ITiler]{
	PureFunc: func() proto.ITiler {
		return tiler.New(Hasher.Get(), Sequencer.Get())
	},
}

var LCompressor = di.Singleton[proto.ILCompressor]{
	PureFunc: l_compressor.New,
}

var LDecompressor = di.Singleton[proto.ILDecompressor]{
	PureFunc: l_decompressor.New,
}

var ArchEncoder = di.Singleton[proto.IArchEncoder]{
	PureFunc: func() proto.IArchEncoder {
		return encoder.New(Config.Get().JpegQuality)
	},
}
