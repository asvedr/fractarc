package app

import (
	"fractarc/entities/config"

	"gitlab.com/asvedr/cldi/cl"
	"gitlab.com/asvedr/cldi/di"
)

var Config = di.Singleton[*config.Config]{
	ErrFunc: cl.BuildConfig[config.Config],
}
